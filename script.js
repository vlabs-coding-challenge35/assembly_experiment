var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var imageList = [
    { x: 450, y: 320, destX: 450, destY: 320, width: 778, height: 284, src: './images/fixed.png', draggable: false },
    { x: 1000, y: 10, destX: 375, destY: 230, width: 142, height: 71, src: './images/item1.png', draggable: true },
    { x: 1000, y: 80, destX: 380, destY: 430, width: 52, height: 99, src: './images/item2.png', draggable: true },
    { x: 700, y: 80, destX: 1050, destY: 140, width: 161, height: 182, src: './images/meter.png', draggable: true },
    { x: 600, y: 80, destX: 365, destY: 300, width: 66, height: 130, src: './images/filter.png', draggable: true },
    { x: 500, y: 80, destX: 380, destY: 530, width: 86, height: 89, src: './images/bee.png', draggable: true },
    { x: 400, y: 80, destX: 560, destY: 80, width: 99, height: 107, src: './images/glass.png', draggable: true },
    { x: 300, y: 80, destX: 375, destY: 55, width: 85, height: 335, src: './images/box.png', draggable: true },
    { x: 100, y: 80, destX: 375, destY: 0, width: 85, height: 335, src: './images/pipe.png', draggable: true },
];
window.onload = function () {
    // canvas related vars
    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    canvas.width = window.innerWidth - 50;
    canvas.height = window.innerHeight - 50;
    var cw = canvas.width;
    var ch = canvas.height;
    document.body.appendChild(canvas);
    // used to calc canvas position relative to window
    function reOffset() {
        var BB = canvas.getBoundingClientRect();
        offsetX = BB.left;
        offsetY = BB.top;
    }
    var offsetX, offsetY;
    reOffset();
    window.onscroll = function (e) { reOffset(); };
    window.onresize = function (e) { reOffset(); };
    canvas.onresize = function (e) { reOffset(); };
    // save relevant information about shapes drawn on the canvas
    var shapes = [];
    // drag related vars
    var isDragging = false;
    var startX, startY;
    // hold the index of the shape being dragged (if any)
    var selectedShapeIndex;
    var _loop_1 = function (imageData) {
        // load the image
        var card = new Image();
        card.onload = function () {
            // define one image and save it in the shapes[] array
            shapes.push(__assign(__assign({}, imageData), { image: card }));
            // draw the shapes on the canvas
            drawAll();
            // listen for mouse events
            canvas.onmousedown = handleMouseDown;
            canvas.onmousemove = handleMouseMove;
            canvas.onmouseup = handleMouseUp;
            canvas.onmouseout = handleMouseOut;
        };
        // put your image src here!
        card.src = imageData.src;
    };
    for (var _i = 0, imageList_1 = imageList; _i < imageList_1.length; _i++) {
        var imageData = imageList_1[_i];
        _loop_1(imageData);
    }
    // given mouse X & Y (mx & my) and shape object
    // return true/false whether mouse is inside the shape
    function isMouseInShape(mx, my, shape) {
        // is this shape an image?
        if (shape.image) {
            // this is a rectangle
            var rLeft = shape.x;
            var rRight = shape.x + shape.width;
            var rTop = shape.y;
            var rBott = shape.y + shape.height;
            // math test to see if mouse is inside image
            if (mx > rLeft && mx < rRight && my > rTop && my < rBott) {
                return (true);
            }
        }
        // the mouse isn't in any of this shapes
        return (false);
    }
    function handleMouseDown(e) {
        // tell the browser we're handling this event
        e.preventDefault();
        e.stopPropagation();
        // calculate the current mouse position
        startX = parseInt(e.clientX) - offsetX;
        startY = parseInt(e.clientY) - offsetY;
        // test mouse position against all shapes
        // post result if mouse is in a shape
        for (var i = 0; i < shapes.length; i++) {
            if (isMouseInShape(startX, startY, shapes[i]) && shapes[i].draggable != false && (Math.abs(shapes[i].x - shapes[i].destX) > 50 || Math.abs(shapes[i].y - shapes[i].destY) > 50)) {
                // the mouse is inside this shape
                // select this shape
                selectedShapeIndex = i;
                // set the isDragging flag
                isDragging = true;
                // and return (==stop looking for 
                //     further shapes under the mouse)
                return;
            }
        }
    }
    function handleMouseUp(e) {
        // return if we're not dragging
        if (!isDragging) {
            return;
        }
        // tell the browser we're handling this event
        e.preventDefault();
        e.stopPropagation();
        // the drag is over -- clear the isDragging flag
        isDragging = false;
        console.log('checking shape', shapes.every(checkShape));
        shapes.forEach(function (shape) {
            console.log('chk', shape, (Math.abs(shape.x - shape.destX) < 50 && Math.abs(shape.y - shape.destY) < 50));
        });
        if (shapes.every(checkShape)) {
            alert('Congratulations! You have completed the experiment successfully!');
        }
    }
    function handleMouseOut(e) {
        // return if we're not dragging
        if (!isDragging) {
            return;
        }
        // tell the browser we're handling this event
        e.preventDefault();
        e.stopPropagation();
        // the drag is over -- clear the isDragging flag
        isDragging = false;
    }
    function checkShape(shape) {
        return (Math.abs(shape.x - shape.destX) < 50 && Math.abs(shape.y - shape.destY) < 50);
    }
    function handleMouseMove(e) {
        // return if we're not dragging
        if (!isDragging) {
            return;
        }
        // tell the browser we're handling this event
        e.preventDefault();
        e.stopPropagation();
        // calculate the current mouse position         
        var mouseX = parseInt(e.clientX) - offsetX;
        var mouseY = parseInt(e.clientY) - offsetY;
        // how far has the mouse dragged from its previous mousemove position?
        var dx = mouseX - startX;
        var dy = mouseY - startY;
        // move the selected shape by the drag distance
        var selectedShape = shapes[selectedShapeIndex];
        selectedShape.x += dx;
        selectedShape.y += dy;
        // clear the canvas and redraw all shapes
        drawAll();
        // update the starting drag position (== the current mouse position)
        startX = mouseX;
        startY = mouseY;
    }
    // clear the canvas and 
    // redraw all shapes in their current positions
    function drawAll() {
        ctx.clearRect(0, 0, cw, ch);
        for (var i = 0; i < shapes.length; i++) {
            var shape = shapes[i];
            if (shape.image) {
                // it's an image
                ctx.drawImage(shape.image, shape.x, shape.y);
            }
        }
    }
};
