# Assembly Experiment
1. This project allows you to build an experiment setup on a HTML canvas.
2. It allows you to drag the images on the canvas.
3. Once the images are in destination proximity they are locked in place.
4. When all the images are in place you get an alert congratulating you for completion of project!

## Setup
Setup is very simple. Just clone the repo and open the html file.